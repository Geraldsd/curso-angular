//////////////////////////////////VAR/////////////////////////////////////


var saludo = "Hola"; //al declarar con var el valor de la variable puede cambiar a lo largo del código

if(true){
    var saludo = "chao";
}

console.log(saludo)

//////////////////////////////////LET/////////////////////////////////////

let saludo2 = "Hola"; //al declarar con let el valor de la variable no cambia dentro de la sentencia
if(true){
    let saludo2 = "chao";

    console.log(saludo2); //se mantiene dentro del scope el valor, ya que solo allí fue declarado.
}

console.log(saludo2)

///////////////////////////////COMNSTANTES////////////////////////////////

const NOMBRE = "juanito"; //el valor de las contantes solo se puede sar al momento de inicializarla.
console.log(NOMBRE);

//si se puede definir dentro de un scoupe, pero valor ser+ia igual que en las variables tipo let (se mantiene dentro del scope el valor)

if(true){
    const NOMBRE = "GERALD";
    console.log(NOMBRE);
};