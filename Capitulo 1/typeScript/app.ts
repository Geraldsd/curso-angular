let nombre = "Gerald";
let apellido = "Seguel";
let edad = 26;

function getNombre(){
    return "Geraldo";
};

//let texto = "Hola "+ nombre; --> forma no correcta de concatenar

let texto:string = `Hola ${nombre} ${apellido} ${edad}`; //forma correcta de concatenar en typescript
let texto2:string =`${1+2}`; //puede resolver operaciones
let texto3:string = `${getNombre()}`; //puede incorporar funciones

console.log(texto);
console.log(texto2);
console.log(texto3);