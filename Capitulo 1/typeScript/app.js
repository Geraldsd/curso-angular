"use strict";
var nombre = "Gerald";
var apellido = "Seguel";
var edad = 26;
function getNombre() {
    return "Geraldo";
}
;
//let texto = "Hola "+ nombre; --> forma no correcta de concatenar
var texto = "Hola " + nombre + " " + apellido + " " + edad; //forma correcta de concatenar en typescript
var texto2 = "" + (1 + 2); //puede resolver operaciones
var texto3 = "" + getNombre(); //puede incorporar funciones
console.log(texto);
console.log(texto2);
console.log(texto3);
